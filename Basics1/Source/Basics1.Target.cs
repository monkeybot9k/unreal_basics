// Unreal Basics 1 By: Nicolas Viegas Palermo (2017).

using UnrealBuildTool;
using System.Collections.Generic;

public class Basics1Target : TargetRules
{
	public Basics1Target(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "Basics1" } );
	}
}
