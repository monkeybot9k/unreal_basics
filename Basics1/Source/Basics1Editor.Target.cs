// Unreal Basics 1 By: Nicolas Viegas Palermo (2017).

using UnrealBuildTool;
using System.Collections.Generic;

public class Basics1EditorTarget : TargetRules
{
	public Basics1EditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "Basics1" } );
	}
}
