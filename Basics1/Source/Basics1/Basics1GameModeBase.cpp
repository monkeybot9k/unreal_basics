// Unreal Basics 1 By: Nicolas Viegas Palermo (2017).

#include "Basics1GameModeBase.h"

#include "MainCharacter.h"

ABasics1GameModeBase::ABasics1GameModeBase(): Super()
{
	
	///	Set our MainCharacter Class as the
	/// default Pawn to be used in the game.
	DefaultPawnClass = AMainCharacter::StaticClass();
}
