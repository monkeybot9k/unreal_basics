// Unreal Basics 1 By: Nicolas Viegas Palermo (2017).

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PawnMovementComponent.h"
#include "WalkingMovementComponent.generated.h"

/**
 * Moves the Owner Pawn at a Walking pace.
 */
UCLASS()
class BASICS1_API UWalkingMovementComponent : public UPawnMovementComponent
{
	GENERATED_BODY()

	// Update Owner Actor Location with the current Input Vector.
	void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction);
};
