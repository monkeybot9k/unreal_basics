// Unreal Basics 1 By: Nicolas Viegas Palermo (2017).

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Basics1GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class BASICS1_API ABasics1GameModeBase : public AGameModeBase
{
	GENERATED_BODY()

		/// Notice the following constructor is not set as public, yet it works without problems.
		/// Unreal will take care of creating instances of all Unreal classes when needed.
		// Default constructor, sets the UMainCharacter as Default Pawn class.
		ABasics1GameModeBase();
	
};
