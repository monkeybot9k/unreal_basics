// Unreal Basics 1 By: Nicolas Viegas Palermo (2017).

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "Manipulable.generated.h"

/**
 * 
 */
UCLASS()
class BASICS1_API AManipulable : public AStaticMeshActor
{
	GENERATED_BODY()
	
	// Sets Default Values
	AManipulable();

	public:
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Manipulation")
	// Called when the Main Character taps the Action key while holding this object.
	void OnGrabbed();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Manipulation")
	// Called when the Main Character releases the Action key while holding this object.
	void OnReleased();
};
