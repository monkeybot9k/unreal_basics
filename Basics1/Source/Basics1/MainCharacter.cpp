// Unreal Basics 1 By: Nicolas Viegas Palermo (2017).

#include "MainCharacter.h"

// Objects that can be grabbed by the player
#include "Manipulable.h"

// Input Component, Used for binidng controls to methods
#include "Components/InputComponent.h"

// Movement Comoponent, applies movement to the Character
#include "WalkingMovementComponent.h"

// Player Controller, Interface between the player and the app
#include "GameFramework/PlayerController.h"

// Camera Manager, used to control camera rotation
#include "Camera/PlayerCameraManager.h"

#include "Engine.h"

// Sets default values
AMainCharacter::AMainCharacter()
{
 	/// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	/// Create PhysicsHandle
	PhysicsHandleComponent = CreateDefaultSubobject<UPhysicsHandleComponent>(TEXT("PhysicsHandle"));
	
	/// Create Movement Component
	MovementComponent = CreateDefaultSubobject<UWalkingMovementComponent>(TEXT("MovementComponent"));

	/// Set Movement Component's Updated Component to be the Character's Root Component.
	MovementComponent->UpdatedComponent = RootComponent;
	
	/// Prevent Rotation from being influenced by PlayerController
	bUseControllerRotationYaw = false;
	bUseControllerRotationPitch = false;
	
	/// Adjust Player Scale
	SetActorScale3D(FVector(1, 1, 1.5));
}

// Called when the game starts or when spawned
void AMainCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	/// Set Main Character as View Target for Player Controller Camera
	/// (unnecessary at this time, since this is set as the camera target by default)
	//GetWorld()->GetFirstPlayerController()->PlayerCameraManager->SetViewTarget(this);
}

void AMainCharacter::CalcCamera(float DeltaTime, struct FMinimalViewInfo& OutResult)
{
	OutResult.Rotation = GetActorRotation();
	OutResult.Location = GetActorLocation();
}

// Called every frame
void AMainCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	/// Update Physics Handle if holding an object
	if (GrabbedObject != nullptr)
	{
		/// Calculate and set Hold Location
		FVector HoldLocation = GetActorLocation() + GetActorForwardVector() * HoldDistance;
		PhysicsHandleComponent->SetTargetLocation(HoldLocation);
	}

	/// Check if we need to move
	if (InputVector.IsNearlyZero()) return;

	/// Update New Location with InputVector's values
	MovementComponent->AddInputVector(GetActorForwardVector() * InputVector.X);
	MovementComponent->AddInputVector(GetActorRightVector() * InputVector.Y);
}

// Called to bind functionality to input
void AMainCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	
	/// Bind Movement
	PlayerInputComponent->BindAxis("Walk", this, &AMainCharacter::Walk);
	PlayerInputComponent->BindAxis("Strafe", this, &AMainCharacter::Strafe);

	/// Bind rotation
	PlayerInputComponent->BindAxis("Yaw", this, &AMainCharacter::Yaw);
	PlayerInputComponent->BindAxis("Pitch", this, &AMainCharacter::Pitch);

	/// Bind Action
	PlayerInputComponent->BindAction("Action", EInputEvent::IE_Pressed, this, &AMainCharacter::ReachForObject);
	PlayerInputComponent->BindAction("Action", EInputEvent::IE_Released, this, &AMainCharacter::ReleaseObject);

}

#pragma region Interaction

// Find the first Manipulable Object within reach and attempt to grab it
void AMainCharacter::ReachForObject()
{
	UE_LOG(LogTemp, Warning, TEXT("Reaching for Object"));

	/// Main Character's Location will be used as Start Vector
	// Current Location
	FVector Start = GetActorLocation();

	/// Main Character's Orientation and reach will be used to calculate End Vector
	// Start + Forward * Reach
	FVector End = Start + GetActorForwardVector() * Reach;

	/// Create empty hit result
	// Hit Result of the linetrace
	FHitResult OutHit(ForceInit);

	// Query Parameters set to ignore Main Character
	FCollisionQueryParams Params = FCollisionQueryParams(FName(TEXT("Reach Trace")), false, this);

	/// Poll the world for the first Dynamic object that intersects the line trace
	if (GetWorld()->LineTraceSingleByObjectType(OutHit, Start, End, FCollisionObjectQueryParams::AllDynamicObjects, Params))
	{
		UE_LOG(LogTemp, Warning, TEXT("Found Object:%s"), *(OutHit.Actor.Get()->GetName()));

		// Object hit by the Linetrace (If Any)
		AManipulable* Object = Cast<AManipulable>(OutHit.Actor.Get());

		/// Attempt grab
		GrabObject(Object);
	}	
}

// Find a Manipulable Object in front of character and grab it.
void AMainCharacter::GrabObject(AManipulable* Object)
{
	if (!Object) return;

	/// Assign the Object as GrabbedObject
	GrabbedObject = Object;

	/// Get the Static Mesh Component so it can be accessed directly
	UStaticMeshComponent* GrabbedObjectMeshComponent = GrabbedObject->GetStaticMeshComponent();

	/// Physics Grab
	PhysicsHandleComponent->GrabComponentAtLocation(GrabbedObjectMeshComponent,TEXT("GrabbedObject"), GrabbedObject->GetActorLocation());
	
	/// Notify Grab
	GrabbedObject->OnGrabbed();

	UE_LOG(LogTemp, Warning, TEXT("Grabbed Object:%s"), *(Object->GetName()));
}

// Release the grabbed object
void AMainCharacter::ReleaseObject()
{
	if (!GrabbedObject) return;
	
	/// Get the Static Mesh Component so it can be accessed directly
	UStaticMeshComponent* GrabbedObjectMeshComponent = GrabbedObject->GetStaticMeshComponent();
		
	/// Release Physics Handle
	PhysicsHandleComponent->ReleaseComponent();

	/// Make sure Component's rigidbody is 
	GrabbedObjectMeshComponent->WakeRigidBody();

	/// Notify Released
	GrabbedObject->OnReleased();

	UE_LOG(LogTemp, Warning, TEXT("Released Object:%s"), *(GrabbedObject->GetName()));

	GrabbedObject = nullptr;
}


#pragma endregion Grab / Release

#pragma region Movement

void AMainCharacter::Walk(float AxisValue)
{
	InputVector.X = FMath::Clamp<float>(AxisValue, -MaxWalkSpeed, MaxWalkSpeed);
}

void AMainCharacter::Strafe(float AxisValue)
{
	InputVector.Y = FMath::Clamp<float>(AxisValue, -MaxWalkSpeed, MaxWalkSpeed);
}

#pragma endregion Walk / Strafe

#pragma region Rotation

void AMainCharacter::Yaw(float AxisValue)
{
	/// Only move on if there was a change on the corresponding Axis Value
	if (AxisValue == 0.0f) { return; }

	/// Get Current Rotation
	FRotator CurrentRotation = GetActorRotation();

	/// Calculate new Rotation
	FRotator NewRotation = CurrentRotation + FRotator(0, AxisValue, 0);

		
	/// Apply rotation to Controlled Pawn
	SetActorRotation(NewRotation);

	//UE_LOG(LogTemp, Warning, TEXT("XValue:%f"), AxisValue);
}

void AMainCharacter::Pitch(float AxisValue)
{
	/// Only move on if there was a change on the corresponding Axis Value
	if (AxisValue == 0.0f) { return; }

	/// Get Current Rotation
	FRotator CurrentRotation = GetActorRotation();

	/// Calculate new Rotation
	FRotator NewRotation = CurrentRotation + FRotator(AxisValue, 0, 0);

	/// Clamp to +- 60 degrees
	NewRotation.Pitch = FMath::Clamp(NewRotation.Pitch, -60.0f, 60.0f);

	/// Apply rotation to Controlled Pawn
	SetActorRotation(NewRotation);

	//UE_LOG(LogTemp, Warning, TEXT("YValue:%f"), AxisValue);
}

#pragma endregion Yaw / Pitch