// Unreal Basics 1 By: Nicolas Viegas Palermo (2017).

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "MainCharacter.generated.h"

class UWalkingMovementComponent;
class AManipulable;

UCLASS()
class BASICS1_API AMainCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMainCharacter();
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void CalcCamera(float DeltaTime, struct FMinimalViewInfo& OutResult);

#pragma region Interaction

	UFUNCTION(BlueprintCallable, Category = "Interaction")
		// Find the first Manipulable Object within reach and attempt to grab it
		void ReachForObject();

	UFUNCTION(Blueprintcallable, Category = "Interaction")
		// Grab a Manipulable Object.
		void GrabObject(AManipulable* Object);

	UFUNCTION(Blueprintcallable, Category = "Interaction")
		// Release the grabbed object
		void ReleaseObject();
#pragma endregion Grab/Release

#pragma region Movement

	UFUNCTION(BlueprintCallable, Category = "PlayerMovement")
		// Moves the Controlled Actor along the X axis, capped to +/- WalkSpeed.
		void Walk(float AxisValue);

	UFUNCTION(BlueprintCallable, Category = "PlayerMovement")
		// Moves the Controlled Actor along the Y axis, capped to +/- StrafeSpeed.
		void Strafe(float AxisValue);

#pragma endregion Walk / Strafe

#pragma region Rotation

	UFUNCTION(BlueprintCallable, Category = "PlayerMovement")
		// Rotates the Controlled Actor along the Z axis (horizontal rotation)
		void Yaw(float AxisValue);

	UFUNCTION(BlueprintCallable, Category = "PlayerMovement")
		// Rotates the Controlled Actor along the Y axis (vertical rotation)
		void Pitch(float AxisValue);

#pragma endregion Pitch / Yaw

protected:
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		// Reach of the Main Character, defines maximum distance at which objects are "grabbable"
		float Reach = 300;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		// Distance at which the Manipulable Object grabbed by the Main Character will be held
		float HoldDistance = 100;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintreadOnly)
		// Object the player is holding at the moment (if any)
		AManipulable* GrabbedObject = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		// Movement Component
		class UWalkingMovementComponent* MovementComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		// Handle used for grabbing Manipulable Objects
		class UPhysicsHandleComponent* PhysicsHandleComponent;
#pragma region Default Speed Values

	UPROPERTY(EditDefaultsOnly)
		// Default amount of cm/s second the player character will move forwards/backwards.
		float MaxWalkSpeed = 0.5f;

	UPROPERTY(EditDefaultsOnly)
		// Default amount of cm/s second the player character will move forwards/backwards.
		float MaxStrafeSpeed = 0.5f;

	UPROPERTY(EditDefaultsOnly)
		// Default rotation speed (in Degrees).
		float MaxRotationSpeed = 360.0f;

#pragma endregion
	
private:
	// Defines actor movement to be applied per frame
	FVector InputVector = FVector::ZeroVector;
	
};
