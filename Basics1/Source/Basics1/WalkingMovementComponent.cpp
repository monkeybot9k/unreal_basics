// Unreal Basics 1 By: Nicolas Viegas Palermo (2017).

#include "WalkingMovementComponent.h"

#include "GameFramework/Pawn.h"

void UWalkingMovementComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Check Update is valid
	if (!UpdatedComponent || ShouldSkipUpdate(DeltaTime))
	{
		return;
	}

	// Intended new movement
	FVector DesiredMovement = ConsumeInputVector() * DeltaTime;

	/// Check if movement is significant
	if (!DesiredMovement.IsNearlyZero())
	{
		// Stores hit result obtained after moving
		FHitResult Hit;

		/// Move the Pawn if there are no obstacles in the way.
		SafeMoveUpdatedComponent(DesiredMovement, GetPawnOwner()->GetActorRotation(), true, Hit);
	}
}

